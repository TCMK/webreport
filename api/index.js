var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.send('--GET-- Hello World /api');
});

router.use('/v1_0', require('./v1_0'));

module.exports = router;