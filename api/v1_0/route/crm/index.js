var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.send('--GET-- Hello World /api/v1_0/crm');
});

// router.use('/manage',require('./manage'));  //ไม่ได้ใช้แล้ว
// router.use('/payment_data',require('./payment_data'));
// router.use('/payment',require('./payment'));
// router.use('/note',require('./note'));
// router.use('/export_data',require('./export_data'));
// router.use('/',require('././route/crm/index'));
// router.use('/',require('../../controller/getBankController'));

module.exports = router;

