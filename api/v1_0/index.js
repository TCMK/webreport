var express = require('express');
var router = express.Router();
 
router.get('/', function(req, res, next) {
  res.send('--GET-- Hello World /api/v1_0');
});

router.use('/crm',require('./route/crm'));
// router.use('/account',require('./route/account'));
// router.use('/public_data',require('./route/public_data'));
 
module.exports = router;