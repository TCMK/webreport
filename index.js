var express = require("express");
var app = express();
var cors = require("cors");
const cron = require("node-cron");
const pg = require("pg");

// global.globalConfig = {
//   user: "postgres",
//   host: "203.150.52.243",
//   database: "postgres",
//   password: "N12jK*Cv",
//   port: 5432,
//   max: 50,
//   idle: 30000
// };


global.globalConfig = {
  user: "postgres",
  host: "localhost",
  database: "iepl",
  password: "123456789",
  port: 5432,
  max: 50,
  idle: 30000
};

const pool = new pg.Pool(globalConfig);

app.get("/", function(req, res) {
  res.send("--GET-- Hello World");
});

// app.get("/json", function(req, res) {
//   res.json({ res: "--GET-- Hello World" });
// });

// app.use(cors());
app.use("/api", require("./api"));

var server = app.listen(4000, function() {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Listening at http://%s:%s", host, port);
});

// // function query
// async function func_query(q, p) {
//   const client = await pool.connect();
//   let res;
//   try {
//     // Begin Transaction
//     await client.query("BEGIN");
//     try {
//       // Query String and Array of Param
//       res = await client.query(q, p);
//       // Commit Transaction
//       await client.query("COMMIT");
//     } catch (err) {
//       // Error >> Rollback Transaction
//       await client.query("ROLLBACK");
//       throw err;
//     }
//   } finally {
//     client.release();
//   }
//   return res;
// }

// function update_status() {
//   try {
//     q = `UPDATE "T_SALEORDER_BILLING_ITEM"
//       SET "STATUS_HOLD" = 3, "STATUS_HOLD_CREATE_AT" = NOW(), "STATUS_HOLD_CREATE_BY" = 'SYSTEM UPDATE EVERYDAY 00:00', "PLAN_TO_PAID" = "T_SALEORDER_BILLING_SEND"."DELIVERY_DATETIME" + INTERVAL '14 day'
//     FROM "T_SALEORDER_BILLING_SEND"
//       WHERE "T_SALEORDER_BILLING_ITEM"."BILL_SEND_CODE" = "T_SALEORDER_BILLING_SEND"."BILL_SEND_CODE"
//         and "T_SALEORDER_BILLING_ITEM"."SO_CODE" = "T_SALEORDER_BILLING_SEND"."SO_CODE"
//         and "T_SALEORDER_BILLING_SEND"."DELIVERY_END_STATUS" = 2
//         and "T_SALEORDER_BILLING_ITEM"."BILL_STATUS" = 4
//         and CAST(now() - INTERVAL '3 day' AS DATE) > CAST("T_SALEORDER_BILLING_SEND"."DELIVERY_DATETIME" as DATE)`;
//     func_query(q, []);
//     console.log("---------- running every 00:00 ----------");
//   } catch (err) {
//     console.log("Database:::::::" + err);
//   }
// }

cron.schedule("00 00 * * *", function() {
  update_status();
});